package com.embracesource.gracefulresponse;

import org.springframework.http.HttpStatus;

public class GracefulResponseException extends RuntimeException {

    private HttpStatus httpStatus;

    private String code;
    private String msg;

    public GracefulResponseException() {
    }

    public GracefulResponseException(HttpStatus httpStatus, String code, String msg) {
        super(msg);
        this.httpStatus = httpStatus;
        this.code = code;
        this.msg = msg;
    }

    public GracefulResponseException(HttpStatus httpStatus, String code, String msg, Throwable cause) {
        super(msg, cause);
        this.httpStatus = httpStatus;
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
