package com.embracesource.gracefulresponse.defaults;


import com.embracesource.gracefulresponse.GracefulResponseProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.embracesource.gracefulresponse.data.ResponseStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.MessageFormat;

/**
 * 默认的ResponseStatus实现
 *
 * @author <a href="mailto:943868899@qq.com">Yujie</a>
 * @version 0.1
 * @since 0.1
 */
public class DefaultResponseStatus implements ResponseStatus {

    @JsonIgnore
    private String format = "{1}";

    @JsonIgnore
    private HttpStatus httpStatus;

    /**
     * 响应码.
     */
    @JsonProperty
    private String errorCode;

    @JsonProperty
    private String errorMsg;

    @JsonIgnore
    private String code;

    public String getErrorCode() {
        return MessageFormat.format(format, httpStatus.value(), code);
    }

    public String getErrorMsg() {
        return msg;
    }

    /**
     * 响应信息.
     */
    @JsonIgnore
    private String msg;

    public DefaultResponseStatus() {
    }

    /**
     * 通过响应码和响应信息构造枚举.
     *
     * @param code 响应码
     * @param msg  响应信息
     */
    public DefaultResponseStatus(HttpStatus httpStatus, String code, String msg) {
        this.httpStatus = httpStatus;
        this.code = code;
        this.msg = msg;
    }

    @Override
    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    @Override
    public HttpStatus getHttpStatus() {
        return this.httpStatus;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String getMsg() {
        return msg;
    }

    public void setFormat(String format) {
        this.format = format;
    }
}
