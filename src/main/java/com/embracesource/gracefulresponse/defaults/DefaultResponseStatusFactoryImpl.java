package com.embracesource.gracefulresponse.defaults;


import com.embracesource.gracefulresponse.GracefulResponseProperties;
import com.embracesource.gracefulresponse.api.ResponseStatusFactory;
import com.embracesource.gracefulresponse.data.ResponseStatus;
import org.springframework.http.HttpStatus;

import javax.annotation.Resource;

/**
 * 提供的默认的ResponseMetaFactory实现.
 *
 * @author <a href="mailto:943868899@qq.com">Yujie</a>
 * @version 0.1
 */
public class DefaultResponseStatusFactoryImpl implements ResponseStatusFactory {

    @Resource
    private GracefulResponseProperties properties;

    @Override
    public ResponseStatus defaultSuccess() {
        DefaultResponseStatus defaultResponseStatus = new DefaultResponseStatus();
        defaultResponseStatus.setFormat(properties.getDefaultErrorcodeFormat());
        defaultResponseStatus.setCode(properties.getDefaultSuccessCode());
        defaultResponseStatus.setMsg(properties.getDefaultSuccessMsg());
        return defaultResponseStatus;
    }

    @Override
    public ResponseStatus defaultError(HttpStatus httpStatus) {
        DefaultResponseStatus defaultResponseStatus = new DefaultResponseStatus();
        defaultResponseStatus.setHttpStatus(httpStatus);
        defaultResponseStatus.setFormat(properties.getDefaultErrorcodeFormat());
        defaultResponseStatus.setCode(properties.getDefaultErrorCode());
        defaultResponseStatus.setMsg(properties.getDefaultErrorMsg());
        return defaultResponseStatus;
    }

    @Override
    public ResponseStatus newInstance(HttpStatus httpStatus, String code, String msg) {
        DefaultResponseStatus defaultResponseStatus = new DefaultResponseStatus(httpStatus, code, msg);
        defaultResponseStatus.setFormat(properties.getDefaultErrorcodeFormat());
        return defaultResponseStatus;
    }
}
