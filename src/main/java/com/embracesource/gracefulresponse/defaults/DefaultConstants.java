package com.embracesource.gracefulresponse.defaults;

/**
 * 默认的响应码和提示信息
 */
public class DefaultConstants {

    /**
     * 默认的成功响应码
     */
    public static final String DEFAULT_SUCCESS_CODE = "200";

    /**
     * 默认的成功提示信息
     */
    public static final String DEFAULT_SUCCESS_MSG = "Success";

    /**
     * 默认的错误码
     */
    public static final String DEFAULT_ERROR_CODE = "000";

    /**
     * 默认的错误提示
     */
    public static final String DEFAULT_ERROR_MSG = "Error";

}
