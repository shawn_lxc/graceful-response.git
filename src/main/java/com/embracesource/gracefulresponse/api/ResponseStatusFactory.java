package com.embracesource.gracefulresponse.api;

import com.embracesource.gracefulresponse.data.ResponseStatus;
import org.springframework.http.HttpStatus;

/**
 * @author <a href="mailto:qinyujie@gingo.cn">Yujie</a>
 * @version 0.1
 */
public interface ResponseStatusFactory {
    /**
     * 获得响应成功的ResponseMeta.
     *
     * @return
     */
    ResponseStatus defaultSuccess();

    /**
     * 获得失败的ResponseMeta.
     *
     * @return
     */
    ResponseStatus defaultError(HttpStatus httpStatus);


    /**
     * 从code和msg创建ResponseStatus
     * @param code
     * @param msg
     * @return
     */
    ResponseStatus newInstance(HttpStatus httpStatus, String code, String msg);

}
