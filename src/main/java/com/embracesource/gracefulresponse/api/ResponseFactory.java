package com.embracesource.gracefulresponse.api;

import com.embracesource.gracefulresponse.data.Response;
import com.embracesource.gracefulresponse.data.ResponseStatus;
import org.springframework.http.HttpStatus;

/**
 * ResponseBean的工厂类，用于生成ResponseBean.
 *
 * @author <a href="mailto:943868899@qq.com">Yujie</a>
 * @version 0.1
 */
public interface ResponseFactory {


    /**
     * 创建新的空响应.
     *
     * @return
     */
    Response newEmptyInstance();

    /**
     * 创建新的空响应.
     *
     * @param statusLine 响应行信息.
     * @return
     */
    Response newInstance(ResponseStatus statusLine);

    /**
     * 创建新的响应.
     *
     * @return
     */
    Response newSuccessInstance();

    /**
     * 从数据中创建成功响应.
     *
     * @param data 响应数据.
     * @return
     */
    Response newSuccessInstance(Object data);

    /**
     * 创建新的失败响应.
     *
     * @return
     */
    Response newFailInstance(HttpStatus httpStatus);

}
